<?php
// Данные отправителя
$fromEmail    = 'test@yandex.ru';
$fromPassword = 'test';
$fromHost     = 'smtp.yandex.ru';
$fromPort     = 465;

// Данные получателя
$toEmail = 'test@mail.ru';

// Данные письма
$mailFrom    = 'МТС Уфа';
$mailTitle   = 'Новая заявка';

require 'phpmailer/PHPMailer.php';
require 'phpmailer/SMTP.php';
require 'phpmailer/Exception.php';

$data = json_decode(file_get_contents('php://input'), true);
$name = $data['name'];
$phone = $data['phone'];

$mail = new PHPMailer\PHPMailer\PHPMailer();
try {
    $mail->isSMTP();
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = 'ssl';
    $mail->CharSet    = "UTF-8";
    $mail->Host       = $fromHost;
    $mail->Username   = $fromEmail;
    $mail->Password   = $fromPassword;
    $mail->Port       = $fromPort;
    $mail->setFrom($fromEmail, $mailFrom);
    $mail->addAddress($toEmail);
    $mail->isHTML(true);
    $mail->Subject = $mailTitle;
    $mail->Body    = "<b>Клиент:</b> $name <br><b>Номер телефона:</b> $phone <br>";
    $result = $mail->send();
    echo $result;
    echo $mail->ErrorInfo;
    if ($result) { echo 'true'; } else { echo 'false'; }
} catch (Exception $e) {
    echo "Сообщение не было отправлено. Причина ошибки: {$mail->ErrorInfo}";
}
?>
