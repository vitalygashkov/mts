import Vue from 'vue'
import Vuex from 'vuex';
import VueAgile from 'vue-agile'
import ElementUI from 'element-ui';
import VueScrollTo from 'vue-scrollto';
import './styles/element.scss';
import App from './App.vue'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faChevronLeft)
library.add(faChevronRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueAgile)
Vue.use(VueScrollTo)
Vue.use(ElementUI);
Vue.use(Vuex);
Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    requisitionOpen: 0
  },
  mutations: {
    setRequisitionState(state, requisitionState) {
      state.requisitionOpen = requisitionState
    }
  },
  getters: {
    requisitionState(state) {
      return state.requisitionOpen
    }
  }
})

new Vue({
  router,
  store: store,
  render: h => h(App)
}).$mount('#app')
